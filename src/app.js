import './styles/normalize.css';
import './styles/theme.sass';

import Barba from 'barba';

document.addEventListener("DOMContentLoaded", () => {

  var tl1 = new TimelineLite();
  var tl2 = new TimelineLite();
  var tl3 = new TimelineLite();
  var tl4 = new TimelineLite();
  var tl5 = new TimelineLite();
  var tl6 = new TimelineLite();
  var tl7 = new TimelineLite();

  tl1.to('.aside-menu-container', 0.5, { width: 512 })
    .reverse();
  tl2.to('.aside-menu__nav-menu-list', 0.5, { x: -100 })
    .reverse();
  tl3.from('.aside-menu-socials', 0, { display: 'none' })
    .to('.aside-menu-socials', 0, { display: 'flex', delay: 0.5 })
    .to('.aside-menu-socials', .2, { opacity: 1 })
    .reverse();
  tl4.to('.aside-menu-full__nav-menu-list', 0, { display: 'block', delay: 0.5 })
    .to('.aside-menu-full__nav-menu-list', .2, { opacity: 1 })
    .reverse();
  tl5.from('.aside-menu-full__nav-menu-list-divider', 0.5, { y: -884, opacity: 0 })
    .reverse();
  tl6.to('.aside-menu__next-button', 0.5, { y: 80, opacity: 0 })
    .reverse();
  tl7.staggerTo(['.aside-menu__copyright', '.aside-menu__light-control'], 0, { delay: 0.5, display: 'flex' })
    .reverse();

  const menuButton = document.querySelector('.aside-menu__button');
  menuButton.addEventListener('click', menuButtonHandler);

  function menuButtonHandler(event) {
    event.preventDefault();
    const menuButton = event.target.tagName === 'BUTTON' ? event.target : event.target.parentNode;


    tl1.reversed() ? tl1.play() : tl1.reverse();
    tl2.reversed() ? tl2.play() : tl2.reverse();
    tl3.reversed() ? tl3.play() : tl3.reverse();
    tl4.reversed() ? tl4.play() : tl4.reverse();
    tl5.reversed() ? tl5.play() : tl5.reverse();
    tl6.reversed() ? tl6.play() : tl6.reverse();
    tl7.reversed() ? tl7.play() : tl7.reverse();

    menuButton.classList.toggle('aside-menu__button__closed');

  }
  ////////////////////


  Barba.Pjax.start();

  Barba.Pjax.init();
  Barba.Prefetch.init();

  Barba.Dispatcher.on('linkClicked', (el) => {
    if (el.classList.contains('aside-menu-full__nav-menu-list-item-page-link') || el.classList.contains('aside-menu__nav-menu-list-item-page-link')) {
      document.querySelectorAll('.page-link__active').forEach((elem) => elem.classList.remove('page-link__active'));
      document.querySelectorAll("." + el.classList[0]).forEach((elem) => elem.classList.add('page-link__active'));
    }
  });


  var FadeTransition = Barba.BaseTransition.extend({
    start: function () {
      /**
       * This function is automatically called as soon the Transition starts
       * this.newContainerLoading is a Promise for the loading of the new container
       * (Barba.js also comes with an handy Promise polyfill!)
       */

      // As soon the loading is finished and the old page is faded out, let's fade the new page
      Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.movePages.bind(this));
    },

    fadeOut: function () {
      /**
       * this.oldContainer is the HTMLElement of the old Container
       */
      var $el = $(this.newContainer);
      $el.css({
        display: 'none',
        width: 0
      });

      TweenLite.to(this.oldContainer, .6, { yPercent: -60 })
      return $(this.oldContainer).animate({ opacity: 0 }, 600).promise();
    },

    fadeIn: function () {
      $(window).scrollTop(0);

      /**
       * this.newContainer is the HTMLElement of the new Container
       * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
       * Please note, newContainer is available just after newContainerLoading is resolved!
       */

      var _this = this;
      var $el = $(this.newContainer);

      $(this.oldContainer).hide();


      $el.css({
        display: 'flex',
        visibility: 'visible',
        opacity: 0
      });

      $el.animate({ opacity: 1 }, 600, function () {
        /**
         * Do not forget to call .done() as soon your transition is finished!
         * .done() will automatically remove from the DOM the old Container
         */
        _this.done();
      });
    }
    ,
    movePages: function () {
      var _this = this;
      var goingForward = true;
      // this.updateLinks();

      if (this.getNewPageFile() === this.oldContainer.dataset.prev) {
        goingForward = false;
      }

      TweenLite.set(this.newContainer, {
        visibility: 'visible',
        yPercent: goingForward ? 60 : -60,
        position: 'fixed',
        left: 0,
        top: 0,
        right: 0
      });
      TweenLite.to(this.oldContainer, 0.6, { yPercent: goingForward ? -60 : 60 });
      TweenLite.to(this.newContainer, 0.6, {
        yPercent: 0, onComplete: function () {
          TweenLite.set(_this.newContainer, { clearProps: 'all' });
          _this.done();
        }
      });
    },

    updateLinks: function () {
      PrevLink.href = this.newContainer.dataset.prev;
      NextLink.href = this.newContainer.dataset.next;
    },

    getNewPageFile: function () {
      return Barba.HistoryManager.currentStatus().url.split('/').pop();
    }



  });

  /**
   * Next step, you have to tell Barba to use the new Transition
   */

  Barba.Pjax.getTransition = function () {
    /**
     * Here you can use your own logic!
     * For example you can use different Transition based on the current page or link...
     */

    return FadeTransition;
  };

});